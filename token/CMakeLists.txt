set(classes
  Compiler
  Hash
  Singletons
  Token
  Type
  TypeContainer
  Manager
  json/jsonToken
  json/jsonManager
)

# Determine whether the cxxabi header exists and if it contains a demangle function.
# XXX(c++17): prefer `__has_include`
check_include_file_cxx("cxxabi.h" HAVE_CXXABI_H)
if (HAVE_CXXABI_H)
  check_cxx_symbol_exists(abi::__cxa_demangle "cxxabi.h" token_HAS_CXXABI_DEMANGLE)
endif()
configure_file(
  "${CMAKE_CURRENT_SOURCE_DIR}/CxxABIConfigure.h.in"
  "${CMAKE_CURRENT_BINARY_DIR}/CxxABIConfigure.h")
list(APPEND headers "${CMAKE_CURRENT_BINARY_DIR}/CxxABIConfigure.h")

foreach (class ${classes})
  list(APPEND headers "${class}.h")
  install(
    FILES "${class}.h"
    DESTINATION "include/token/${token_VERSION}/token/${headerDir}"
  )
  if (EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/${class}.cxx")
    list(APPEND sources "${class}.cxx")
  endif()
endforeach()

# Note that we REQUIRE the token library to be a shared library.
# This is because on some platforms (macos), if multiple executable
# units (i.e., shared libraries) link to the same static token library,
# then token::Token::getManager() will return pointers to different
# instances, causing odd behavior and crashes when strings interned
# by one library are not available to others.
add_library(token SHARED ${sources} ${headers})
target_link_libraries(token
  PUBLIC
    nlohmann_json
)
target_include_directories(token
  PUBLIC
    $<BUILD_INTERFACE:${token_SOURCE_DIR}>
    $<BUILD_INTERFACE:${token_BINARY_DIR}>
    $<INSTALL_INTERFACE:include/token/${token_VERSION}>
)
token_install_library(token)
generate_export_header(token EXPORT_FILE_NAME Exports.h)
install(
  FILES ${CMAKE_CURRENT_BINARY_DIR}/Exports.h
  DESTINATION include/token/${token_VERSION}/token
)

add_executable(tokenize
  tokenize.cxx
)
target_link_libraries(tokenize
  PUBLIC
    token
)
token_install_target(tokenize)

add_subdirectory(testing)
